package com.stackoverflow.rusin.mirek;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * This is a swipe control listener taken from stack overflow users mirek rusin
 * You can find it here: http://stackoverflow.com/questions/4139288/android-how-to-handle-right-to-left-swipe-gestures/12938787#12938787
 */
public abstract class OnSwipeTouchListener implements View.OnTouchListener {

    private final GestureDetector gestureDetector;
    private boolean isOnClick;
    float[] startPoint = new float[2];
    long startTime;

    public OnSwipeTouchListener (Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
    }

    public abstract void onClick(View v);

    public boolean onTouch(View v, MotionEvent event) {

//        Log.i("OnSwipeTouchListener", "Event: " + event);
        switch (event.getAction())
        {
            case MotionEvent. ACTION_DOWN:
            {
                isOnClick = true; // This can be removed as it is ineffective
                startPoint[0] = event.getX();
                startPoint[1] = event.getY();
                startTime = event.getEventTime();
                break ;
            }
            case MotionEvent. ACTION_MOVE:
            {
  //              isOnClick= false;
                break;
            }
            case MotionEvent. ACTION_UP:
            {
                int d = 10;
                boolean distanceCheck = ((Math.abs(event.getX()-startPoint[0]) < d) & Math.abs(event.getY()-startPoint[1])<d);
                boolean timeCheck = event.getEventTime()-startTime<600; // Less than 600ms
//                if(isOnClick)
//                Log.i("OnSwipeListener", String.format("Distance: %s", distanceCheck));
//                Log.i("OnSwipeListener", String.format("Time: %s", timeCheck));
                if (distanceCheck&timeCheck)
                {
//                    Log.i("OnSwipeListener", "CLICK!");
                    //Call your own click listener
                    onClick(v);
                    return false;
                }
                break;
            }
        }
//        return true; // We are interested in receiving more events from this gesture
        return gestureDetector.onTouchEvent(event); //This was only line in onTouch before click check
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 15; //(30 good) was 100
        private static final int SWIPE_VELOCITY_THRESHOLD = 110; // was 100

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                    }
                    result = true;
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                }
                result = true;

            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public abstract void onSwipeRight();

    public abstract void onSwipeLeft();

    public abstract void onSwipeTop();

    public abstract void onSwipeBottom();
}