package com.kaznowski.hugh.ludumcompendium;

import android.provider.DocumentsContract;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Hugh on 20/03/2015.
 */
public class XMLUtilities
{
    /**
     * Create an input stream from the zip stream so that the zip stream isn't closed once it is finished reading
     * @param zis The ZipInputStream pointing to the current file to buffer
     * @return An InputStream containing only the current file from the ZipInputStream
     * @throws java.io.IOException
     */
    public static InputStream zipStreamToInputStream(ZipInputStream zis) throws IOException {
        //Create stream with only this file (that way zip stream doesn't get closed by XML. Alternative solution is to overwrite XML stream close method.

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int length=0;
        while ((length=zis.read(buffer))!=-1)
        {
            baos.write(buffer,0,length);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }

    public static Map<String, String> getNodeAttributes(Node n)
    {
        Map<String, String> attr = new HashMap<>();
        NamedNodeMap rawNodes = n.getAttributes();
        for (int i=0; i<rawNodes.getLength(); i++)
        {
            Node a = rawNodes.item(i);
            if (a.getNodeType()==Node.ATTRIBUTE_NODE)
                attr.put(a.getNodeName(), a.getNodeValue());
        }

        return attr;
    }

    /**
     * Returns all text in node (assuming no nodes are nested)
     * @param n Node to return info for
     * @return String contained in node
     */
    public static String getNodeText(Node n)
    {
        if (n.getNodeType()==Node.ELEMENT_NODE)
        {
            return n.getTextContent();
        }
        throw new RuntimeException("Called on non-element type node");
    }

    public static Document getDocumentFromStream(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = factory.newDocumentBuilder();
        InputSource inputSource = new InputSource(is);
        Document document = db.parse(inputSource);
        return document;
    }

    /**
     * Return the first instance of a tag in a nodelist
     * @param n node to be searched
     * @param tag the tag which node you want to search for
     * @return
     */
    public static Node getSubNode(Node n, String tag)
    {
        NodeList l = n.getChildNodes();
        for (int i=0; i<l.getLength(); i++)
        {
            if (l.item(i).getNodeType()==Node.ELEMENT_NODE)
            {
                if (l.item(i).getNodeName().equals(tag))
                {
                    return l.item(i);
                }
            }
        }
        return null;
    }

    /**
     * Get a list of Nodes nested in given Node that are tag
     * @param n Node being checked
     * @param tag desired XML tag
     * @return list of sub nodes matching criteria
     */
    public static List<Node> getSubNodes(Node n, String tag)
    {
        List<Node> list = new ArrayList<>();
        NodeList l = n.getChildNodes();
        for (int i=0; i<l.getLength(); i++)
        {
            if (l.item(i).getNodeType()==Node.ELEMENT_NODE)
            {
                if (l.item(i).getNodeName().equals(tag))
                {
                    list.add(l.item(i));
                }
            }
        }
        return list;
    }

}
