package com.kaznowski.hugh.ludumcompendium;

public enum TableCellType
{
    TEXT,
    NUMBER, // Potentially used for sorting
    IMAGE;
}
