package com.kaznowski.hugh.ludumcompendium;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by hugh on 02/05/15.
 */
public class DetailInfoTable
{
    String name;
    Map<String, TableCellType> columnTypes;
    List<String> columnOrder;
    List<Object[]> rows;

    public DetailInfoTable(String name)
    {
        columnTypes=new HashMap<>();
        columnOrder=new LinkedList<>();
        this.name=name;
        rows = new LinkedList<>();
    }

    public void addColumn(String name, TableCellType t)
    {
        columnOrder.add(name);
        columnTypes.put(name, t);
    }

    /**
     * Add an entire row of data. Array of Object is taken, but checks are done for size and type
     * @param objects
     */
    public void addRow(Object... objects)
    {
        if (objects.length!=columnOrder.size()) throw new RuntimeException("Row columns and table columns don't match");
        for (int i=0; i<columnOrder.size(); i++)
        {
            String colName = columnOrder.get(i);
            if (!compareType(objects[i], columnTypes.get(colName)))
            {
                throw new RuntimeException(String.format("Bad argument type for column %d (%s), was %s expected %s",
                        i, colName, objects[i].getClass().toString(), columnTypes.get(colName)));
            }
        }

        // At this point we have checked types and order etc, now insert
        rows.add(objects);
    }

    /**
     * Return true if Object o can be interpreted as cell type t {TEXT, NUMBER, IMAGE}
     * @param o
     * @param t
     * @return
     */
    public static boolean compareType(Object o, TableCellType t)
    {
        if (t==TableCellType.TEXT)
        {
            if (o instanceof String == false)
            {
                return false;
            }
            return true;
        }
        else if (t == TableCellType.NUMBER)
        {
            // TODO Technically should check for integer, but so far not implemented
            if (o instanceof Double == false & !o.getClass().isInstance(Double.class))
            {
                return false;
            }
            return true;
        }
        else if (t == TableCellType.IMAGE)
        {
            if (o instanceof Bitmap == false)
            {
                return false;
            }
            return true;
        }
        return false; // Return false if we don't know how to handle type
    }

}

