package com.kaznowski.hugh.ludumcompendium;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Intent Bundle parameters
 * gameID = id of Game class being used
 */
public class GameOverview extends ActionBarActivity {

    private Game g;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_overview);

        Bundle b = getIntent().getExtras();
        int gameID = (Integer) b.get("gameID");
        g = Game.getGameByID(gameID);

        LinearLayout layoutV = (LinearLayout) findViewById(R.id.overview_background);
        layoutV.setBackgroundColor(g.getColorTheme().bodyColour);

        LinearLayout bannerV = (LinearLayout) findViewById(R.id.overview_banner);
        bannerV.setBackgroundColor(g.getColorTheme().titleColour);

        ImageView imgV = (ImageView) findViewById(R.id.overview_thumbnail);
        imgV.setImageBitmap(g.getCover());

        TextView textV = (TextView) findViewById(R.id.overview_title);
        textV.setText(g.getTitle());
        textV.setTextColor(g.getColorTheme().titleText);

        TextView tv = new TextView(this);
        ColorTheme t = g.getColorTheme();
        tv.setText(String.format("This is sample text from GameOverview onCreate. Body Colour/Text Similarity=%d", t.getColourSimilarity(t.bodyColour, t.bodyText)));
        tv.setTextColor(t.bodyText);
        layoutV.addView(tv);

        // TODO below is code for dummy button, deal with it
        for (final String tableName: g.getListOfDetailInfoTableNames()) {
            Button detailsButton = new Button(this);
            detailsButton.setText(tableName);
            detailsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent;
                    intent = new Intent(GameOverview.this, GameInfoTable.class);
                    Bundle b = new Bundle();
//                b.putString("gameName", g.getTitle());
                    b.putInt("gameID", g.getID());
                    b.putString("tableName", tableName);
                    intent.putExtras(b);

                    //ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(GameSelection.this, view.findViewById(R.id.game_cover_tile_image), "cover");

                    //startActivity(intent, options.toBundle());
                    startActivity(intent);
                }
            });
            layoutV.addView(detailsButton);
        }

        // TODO temporary code for talent tree, handles only dummies
        for (String treeName: g.getTalentTreeNames())
        {
            Button button = new Button(this);
            button.setText(treeName);
            Log.i("GameOverview.onCreate", "Button/tree name is " + treeName);
            final String talentTreeName = treeName;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(GameOverview.this, TalentTreeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("gameID", g.getID());
                    bundle.putString("talentTreeName", talentTreeName);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
            layoutV.addView(button);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
