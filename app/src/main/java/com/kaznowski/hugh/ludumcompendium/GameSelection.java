package com.kaznowski.hugh.ludumcompendium;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;


public class GameSelection extends ActionBarActivity {

    private static boolean gamesHaveBeenLoaded=false;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_game_selection);

            GridView grid = (GridView) findViewById(R.id.gridView);
            grid.setAdapter(new GameCoverListAdapter(this));
            grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Toast.makeText(getApplicationContext(), String.format("%d: %s", position, Game.getGameByID(position).getTitle()), Toast.LENGTH_SHORT).show();
//                    RelativeLayout rel = (RelativeLayout) findViewById(R.id.activity_game_selection_rellayout);
//                    GameCoverListAdapter adapter = (GameCoverListAdapter) parent.getAdapter();
//                    Palette p=adapter.getPaletteByID(position);
//                    rel.setBackgroundColor(p.getDarkVibrantColor(0));
                    Intent intent;
                    intent = new Intent(GameSelection.this, GameOverview.class);
                    Bundle b = new Bundle();
                    b.putInt("gameID", position);
                    intent.putExtras(b);

                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(GameSelection.this, view.findViewById(R.id.game_cover_tile_image), "cover");


                    startActivity(intent, options.toBundle());
                }
            });

            loadGames();

            //Set Background colour
            RelativeLayout l = (RelativeLayout) findViewById(R.id.activity_game_selection_rellayout);
            l.setBackgroundColor(Color.GRAY);

        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Games are loaded on the creation of the activity. If the users changes perspective, the onCreate is called again.
     * Having this method separate from the construction and surrounding it with the condition stops the program from loading and creating duplicate games.
     */
    private void loadGames()
    {
        if (!gamesHaveBeenLoaded)
        {
            gamesHaveBeenLoaded=true;
            // Load a sample zip test
            try {
                for (String filename: getResources().getAssets().list(""))
                {
                    if (filename.endsWith(".zip"))
                    {
                        Game g = new Game(filename, getResources());
                        //Log.i("GameSelection", "Finished loading game");
                        //Log.i("GameSelection", "Title: " + g.getTitle());
                        //Log.i("GameSelection", "Bitmap dimensions: " + g.getCover().getWidth() + "x" + g.getCover().getHeight());
                    }
                }
                //Log.i("GameSelection", String.format("Loaded %d games", Game.getGameCount()));
            } catch (IOException e) {
                e.printStackTrace();
            }
//            Toast t = Toast.makeText(getApplicationContext(), "Loaded games", Toast.LENGTH_SHORT);
//            t.show();
        }
    }
}
