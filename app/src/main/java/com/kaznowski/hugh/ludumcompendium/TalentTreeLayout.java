package com.kaznowski.hugh.ludumcompendium;

public enum TalentTreeLayout
{
    FIXED, CENTERED, GRID
}
