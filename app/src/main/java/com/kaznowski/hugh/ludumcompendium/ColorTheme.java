/**
 * Created by hugh on 25/04/15.
 */
package com.kaznowski.hugh.ludumcompendium;


import android.graphics.Color;
import android.support.v7.graphics.Palette;
import android.util.Log;

/**
 * Simple class for colour management
 */
public class ColorTheme
{
    public int titleText= Color.MAGENTA;
    public int titleColour=Color.MAGENTA;
    public int bodyText=Color.MAGENTA;
    public int bodyColour=Color.MAGENTA;

    public static final int COLOUR_MODE=1;

    public ColorTheme(Palette palette)
    {
//        Palette.Swatch swatch = palette.getVibrantSwatch();
        if (COLOUR_MODE==0) // This isnt set by any options, defines colour picking behaviour once compiled
        {
            Palette.Swatch swatch = palette.getVibrantSwatch();

            if (swatch==null)
            {
                // We are dealing with some bad colours here (ex. mostly black)
                bodyColour = palette.getDarkVibrantColor(bodyColour);
    //            bodyColour = getPalette().getDarkMutedColor(bodyColour);
                bodyText = palette.getLightVibrantColor(bodyText);
                titleColour = palette.getDarkVibrantColor(titleColour);
                titleText = palette.getLightVibrantColor(titleText);
            }
            else {
                /*
                int rgb = swatch.getRgb();
                double factor=0.4; // % hue of title colour
                double bcR = Color.red(rgb)*factor;
                double bcG = Color.green(rgb)*factor;
                double bcB = Color.blue(rgb)*factor;
                int bc = Color.rgb((int)bcR, (int)bcG, (int)bcB);
    //            bc=Color.MAGENTA;
                */


                //            bodyColour = palette.getDarkVibrantColor(Color.MAGENTA);
                bodyColour = averageColours(palette.getDarkVibrantColor(Color.MAGENTA), Color.BLACK);
                bodyText = swatch.getBodyTextColor();
                titleColour = swatch.getRgb();
                titleText = swatch.getTitleTextColor();
            }
        }
        else if (COLOUR_MODE==1)
        {
//            titleColour = palette.getVibrantColor(Color.MAGENTA);
            titleColour= palette.getDarkVibrantColor(Color.MAGENTA);
            titleText = palette.getVibrantColor(Color.MAGENTA);
            bodyColour = palette.getMutedColor(Color.MAGENTA);
            bodyColour = averageColours(Color.WHITE, bodyColour);
//            bodyText = palette.getLightMutedColor(Color.MAGENTA);
            bodyText = averageColours(bodyColour, titleColour);
            Log.i("ColorTheme", String.format("Similarity: %d%%", getColourSimilarity(titleColour, bodyColour)));
        }
    }

    /**
     * Returns % (int) of how similar the colours are
     * @param c1
     * @param c2
     * @return
     */
    public static int getColourSimilarity(int c1, int c2)
    {
        int r = Math.abs(Color.red(c2)-Color.red(c1));
        int g = Math.abs(Color.green(c2)-Color.green(c1));
        int b = Math.abs(Color.blue(c2)-Color.blue(c1));
        double avg = (r+g+b)/3;
        avg=255-avg; //The average is really low if similar, we want the opposite
        avg=avg/255*100; //Convert to %
        return (int)avg;
    }

    public static int oppositeColor(int c)
    {
        int r = 255-Color.red(c);
        int g = 255-Color.green(c);
        int b = 255-Color.blue(c);
        return Color.rgb(r, g, b);
    }

    public static int averageColours(int c1, int c2)
    {
        int r = (Color.red(c1)+Color.red(c2))/2;
        int g = (Color.green(c1)+Color.green(c2))/2;
        int b = (Color.blue(c1)+Color.blue(c2))/2;
        return Color.rgb(r, g, b);
    }

    public static int getFactorColor(int c, double factor)
    {
        double bcR = Color.red(c)*factor;
        double bcG = Color.green(c)*factor;
        double bcB = Color.blue(c)*factor;
        return Color.rgb((int)bcR, (int)bcG, (int)bcB);
    }
}
