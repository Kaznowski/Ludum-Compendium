package com.kaznowski.hugh.ludumcompendium;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Hugh on 18/03/2015.
 */
public class GameCoverListAdapter extends BaseAdapter {
    HashMap<String, Integer> game_covers = new HashMap<String, Integer>();
    ArrayList<String> game_ids = new ArrayList<>();
    Context context=null;
//    HashMap<Integer, String> game_ids = new HashMap<>();
//    private static int nextID=0;

    public GameCoverListAdapter(Context c) {
        this();
        context=c;
    }

    private GameCoverListAdapter()
    {
//        addGame("Borderlands 2", R.drawable.borderlands2_500x300);
//        addGame("Starcraft 2", R.drawable.sc2_500x300);
//        addGame("The Elder Scrolls V: Skyrim", R.drawable.skyrim_500x300);
//        addGame("Borderlands", R.drawable.borderlands1_500x300);
//        addGame("Borderlands: The Pre-Sequel", R.drawable.borderlands_pre_seq_500x300);
//        addGame("Fallout 3", R.drawable.fallout3_500x300);
//        addGame("Diablo 3", R.drawable.diablo3_500x300);
    }

    public Palette getPaletteByID(int position)
    {
        return Palette.generate(Game.getGameByID(position).getCover());
    }

//    public void addGame(String name, int drawableID) {
//        int id = nextID++;
//        game_ids.put(id, name);
//        game_ids.add(name);
//        game_covers.put(name, drawableID); //TODO check existing key
//    }

    @Override
//    public int getCount() {
//        return game_ids.size();
//    }
    public int getCount() {
        return Game.getGameCount();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        //TODO range check
        return position; //TODO fix this
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO what happens if game is removed? Game.id doesnt point to anything...
        Game g = Game.getGameByID(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
//        LayoutInflater inflater = getLayoutInflater();
//                    View v = findViewById(R.id.game_cover_tile_layout);
        View v = inflater.inflate(R.layout.game_cover_tile, parent, false);
        v.setBackgroundColor(g.getColorTheme().titleColour);

        TextView textV = (TextView) v.findViewById(R.id.game_cover_tile_title);
        textV.setText(g.getTitle()); //Games title
//        textV.setTextColor(g.getFontColor());
        textV.setTextColor(g.getColorTheme().titleText);

        ImageView imgV = (ImageView) v.findViewById(R.id.game_cover_tile_image);
        imgV.setImageBitmap(g.getCover()); //Games cover
        return v;
    }
}

