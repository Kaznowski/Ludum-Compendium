package com.kaznowski.hugh.ludumcompendium;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

/**
 * This used to be the table class, but has become a class that loads the DetailInfoTable from the
 * file. That's why it is a bit strange in its implementation
 * Created by Hugh on 20/03/2015.
 */
public class Table
{
    DetailInfoTable infoTable;
    Game g;

    public Table(InputStream tableStream, Game g)
    {
        this.g = g;
        Document doc=null;
        try {
            doc = XMLUtilities.getDocumentFromStream(tableStream);
        } catch (ParserConfigurationException e) {
            e.printStackTrace(); // TODO deal with it (⌐■_■)
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        if (doc==null) throw new RuntimeException("Read the stack trace");

        Node tableNode = doc.getElementsByTagName("table").item(0); // This should be the root node, so reading only first is fine

        String name = "placeholder name";
        DetailInfoTable dit = new DetailInfoTable(name);

        // First load and count columns
        Node description = XMLUtilities.getSubNode(tableNode, "description");
        List<Node> hColumns = XMLUtilities.getSubNodes(description, "column");
        for (Node n: hColumns)
        {
            // We are now working with a column definition, defining the DetailInfoTable
            Map<String, String> atts = XMLUtilities.getNodeAttributes(n);
            String type = atts.get("type"); // We get the type of the column we are working with
            TableCellType cellType=null;

            if (type.equalsIgnoreCase("text"))
            {
                cellType=TableCellType.TEXT;
            }
            else if (type.equalsIgnoreCase("number"))
            {
                cellType=TableCellType.NUMBER;
            }
            else if (type.equalsIgnoreCase("image"))
            {
                cellType=TableCellType.IMAGE;
            }
            else throw new RuntimeException("Unknown TableCellType "+ type);

            // Now that we have the cell type, we need the name
            String colName=XMLUtilities.getNodeText(n);

            // We now insert this information into the DIT
            dit.addColumn(colName, cellType);
        }

        // At this point DIT has the table information, we can start processing entries
        List<Node> entries = XMLUtilities.getSubNodes(tableNode, "row");
        for (Node entry: entries)
        {
            // We are going to cross reference these columns with the DIT
            // We will construct a fixed size array of Object and fill it with appropriate class types (String, double, Bitmap, [null?])
            List<Node> fields = XMLUtilities.getSubNodes(entry, "column");
            Object[] objects = new Object[dit.columnOrder.size()];
            for (int i=0; i<dit.columnOrder.size(); i++) // TODO check sizes, 2 arrays {xml, dit}
            {
                String cName=dit.columnOrder.get(i); // This is the column name for the current field
                String rawValue = XMLUtilities.getNodeText(fields.get(i)); // This is the value for the field
                Object value = convertStringToTypeObject(rawValue, dit.columnTypes.get(cName)); // Convert the string to a class value
                if (!DetailInfoTable.compareType(value, dit.columnTypes.get(cName))) //TODO there a so many checks that this could probably be avoided
                {
                    throw new IllegalArgumentException("Somehow after converting a value from XML to a class, the check failed");
                }
                // So far so good, add to array and iterate
                objects[i] = value;
            }
            // We now have our entry, put it into DetailInfoTable
            dit.addRow(objects);
        }
        // Processing table complete
        infoTable=dit;
    }

    /**
     * Try to convert String representation to an actual class (No template, we operate on Object)
     * @param value
     * @param type
     * @return
     * @throws IllegalArgumentException
     */
    public Object convertStringToTypeObject(String value, TableCellType type) throws IllegalArgumentException
    {
        if (type==TableCellType.TEXT)
        {
            return value;
        }
        else if (type==TableCellType.NUMBER)
        {
            try
            {
                Object ret = Double.valueOf(value);
            }
            catch (NumberFormatException e)
            {
                throw new IllegalArgumentException(value+" could not be cast as double");
            }
        }
        else if (type==TableCellType.IMAGE)
        {
            Bitmap bmp=null;
            try
            {
                InputStream is = g.loadFilesFromZip(value).get(value);
                bmp = BitmapFactory.decodeStream(is);

            } catch (IOException e) {
                e.printStackTrace();
                throw new IllegalArgumentException("There was an IOException, but it was easier to throw an IAException." +
                        " This happened while reading the zip or reading the bitmap in the zip");
            }
            finally {
                return bmp;
            }
        }
        throw new IllegalArgumentException("There was a problem processing input for " + value);
    }

    public DetailInfoTable getDetailInfoTable()
    {
        return infoTable;
    }
}
