package com.kaznowski.hugh.ludumcompendium;

import android.app.Dialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.stackoverflow.rusin.mirek.OnSwipeTouchListener;

import org.w3c.dom.Text;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;


public class TalentTreeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_talent_tree);
        TextView tv;

        final LinearLayout mainLayer = (LinearLayout) findViewById(R.id.talent_tree_lin_layout);
//        ScrollView mainLayer = (ScrollView) findViewById(R.id.talent_tree_scrollview); // should be called mainView, but dont want ot change code
        mainLayer.setOrientation(LinearLayout.HORIZONTAL);
//        tv = new TextView(this);
//        tv.setText("At least the activity launched");
//        mainLayer.addView(tv);


        Bundle b=getIntent().getExtras();
        final Game g = Game.getGameByID((Integer) b.get("gameID"));
        TalentTreeLayout treeLayout = TalentTreeLayout.CENTERED; // TODO debug
//        String talentTreeName = "someTree";
        String talentTreeName = (String) b.get("talentTreeName");
        TalentTree tree = g.getTalentTreeByName(talentTreeName);

        if (treeLayout==TalentTreeLayout.CENTERED)
        {
            // We already have a vertical linear layout, we will add horizontal layouts with a few talents
//            Collection<Talent> talents = tree.idTalentMap.values(); // TODO if we use tree.getLayer this isnt required

            //TODO get layer AND group
            List<String> groupNames = tree.getGroups();
            for (String groupName: groupNames)
            {
                LinearLayout groupLayout = new LinearLayout(this);
                groupLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                groupLayout.setOrientation(LinearLayout.VERTICAL);
                tv = new TextView(this);
                tv.setText(groupName);
                tv.setTextColor(g.getColorTheme().titleColour);
                groupLayout.addView(tv);
                for (int layerIn: tree.getLayers())
                {
                    LinearLayout layer = new LinearLayout(this);

                    layer.setOrientation(LinearLayout.HORIZONTAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layer.setLayoutParams(params);

                    List<Talent> talents = tree.getLayer(groupName, layerIn);
                    for (Talent t : talents) {
//                        LinearLayout talLayout = (LinearLayout) findViewById(R.id.talent_lin_layout);
                        LinearLayout talLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.talent_button_layout, null);
                        final Talent finalTalent=t; // This is for dialog

                        TextView titleV = (TextView) talLayout.findViewById(R.id.talent_name);
                        titleV.setText(t.name);

                        ImageView imgV = (ImageView) talLayout.findViewById(R.id.talent_imageView);
                        imgV.setImageDrawable(getDrawable(R.drawable.abc_switch_thumb_material));

                        final ProgressBar progBar = (ProgressBar) talLayout.findViewById(R.id.talent_progressBar);
                        progBar.setMax(t.descriptions.size()-1);
                        progBar.setProgress(0);

//                        Button button = new Button(this);
//                        button.setText(String.format("%s\n[%s:%d]", t.name, t.group, t.layer));
//                        layer.addView(button);
                        talLayout.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {

                            @Override
                            public void onClick(View v) {

//                                Toast.makeText(v.getContext(), "Was click", Toast.LENGTH_SHORT).show();
//
//                                final PopupWindow pw = new PopupWindow(v.getContext());
//                                pw.setContentView(getLayoutInflater().inflate(R.layout.talent_tree_popup_layout, null));

                                final Dialog pw = new Dialog(v.getContext());
                                pw.setContentView(R.layout.talent_tree_popup_layout);

                                final TextView title = (TextView) pw.findViewById(R.id.talent_popup_title);
                                TextView description = (TextView) pw.findViewById(R.id.talent_popup_description);
                                Button button = (Button) pw.findViewById(R.id.talent_popup_close_button);

//                                title.setText("Title");
                                title.setText(finalTalent.name);
//                                title.setBackgroundColor(g.getColorTheme().titleColour);
                                title.setTextColor(g.getColorTheme().titleText);
//                                description.setText("Description");
                                description.setText(finalTalent.descriptions.get(progBar.getProgress()));

                                button.setText("Close");

                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pw.dismiss();
                                    }
                                });

//                                pw.showAsDropDown(v);
                                pw.show();
                            }

                            @Override
                            public void onSwipeRight() {
                                onSwipeTop();
                            }

                            @Override
                            public void onSwipeLeft() {
                                onSwipeBottom();
                            }

                            @Override
                            public void onSwipeTop() {
                                int prog = progBar.getProgress();
                                progBar.setProgress(Math.min(prog + 1, progBar.getMax() + 1)); // Otherwise max never achieved
                            }

                            @Override
                            public void onSwipeBottom() {
                                int prog = progBar.getProgress();
                                progBar.setProgress(Math.max(prog - 1, 0));
                            }
                        });
                        layer.addView(talLayout);
                    }
                    groupLayout.addView(layer);
                }
                mainLayer.addView(groupLayout);
            }
        }
        else
        {
            tv = new TextView(this);
            tv.setText("Somehow, tree layout wasnt set");
            mainLayer.addView(tv);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_talent_tree, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

