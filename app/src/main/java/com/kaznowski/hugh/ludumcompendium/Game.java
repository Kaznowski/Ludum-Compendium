package com.kaznowski.hugh.ludumcompendium;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Hugh on 18/03/2015.
 */
public class Game {
    private static HashMap<Integer, Game> idTable = new HashMap<>();
    private static int nextID=0;
    private int id;

//    private Integer vibrantMuted=null, vibrant=null, darkVibrant=null;
//    private Integer darkMuted=null, dark=null, dark; // Integer instead of int so that null can be used
    private Palette coverPalette = null;
    private ColorTheme theme = null;

    private String title=""; // Required title of game
    private Bitmap cover=null; // Required thumbnail loaded from zip. Filename of thumbnail is cover_500x300.png
    private static Resources resourceManager=null; // This is required if loading from resource manager (should be removed once internal storage and online service are added)
    private String filename;
    private HashMap<String, String> tableFiles = new HashMap<>(); // Table name to filename map
    private HashMap<String, Table> detailTables = new HashMap<>(); // table name to Table (factory for detail info map) (load on demand)
    private HashMap<String, TalentTree> talentTrees = new HashMap<>(); // Map name: tree

    /**
     * Load from resource manager
     * @param name
     * @param resMan
     * @throws IOException
     */
    public Game(String name, Resources resMan) throws IOException {
        resourceManager=resMan;
        filename = name;
        talentTrees=new HashMap<>();
        initialise(); // read the main files from the zip

        // At this point we have loaded the important parts of the file
        // Now generate the palette (colour theme) from the cover image
        getPalette(); // returns a palette, but stores it also

        // Generate colour theme
        theme = new ColorTheme(getPalette());
    }

    private void initialise()
    {
        try
        {
            Map<String, InputStream> files = loadFilesFromZip("details.xml", "cover_500x300.png");
            readDetailsFile(files.get("details.xml"));
            readCoverFile(files.get("cover_500x300.png"));
            if (title.equals("") | cover==null) throw new RuntimeException("Zip file didn't contain a title and cover");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new RuntimeException("There was a problem while processing the zip file");
        }

        id=nextID++;
        idTable.put(id, this);
    }

    /**
     * Load selected files from zip, returning InputStreams that can be used to load XML or bitmaps
     * @param filenames
     * @return Map filename:InputStream
     * @throws IOException
     */
    public HashMap<String, InputStream> loadFilesFromZip(String... filenames) throws IOException
    {
        HashMap<String, InputStream> fileStreams = new HashMap<>();
        InputStream is = resourceManager.getAssets().open(filename);
        ZipInputStream zipIS = new ZipInputStream(is);
        ZipEntry entry = null;
        while ( (entry=zipIS.getNextEntry()) !=null )
        {
            // iterate through filenames and compare
            for (String f: filenames)
            {
                if (entry.getName().equals(f))
                {
                    fileStreams.put(f, XMLUtilities.zipStreamToInputStream(zipIS));
                    break;
                }
            }
            zipIS.closeEntry();
        }
        zipIS.close();
        return fileStreams;
    }


    /**
     * Once the ZipInputStream from the constructor reaches the info file, this method should process it
     * @param is The stream pointing to the current zip
     */
    private void readDetailsFile(InputStream is)
    {
        try {
            Document document = XMLUtilities.getDocumentFromStream(is);

            Node gamenode = document.getElementsByTagName("game").item(0); // TODO check assumption that exists and is only 1
            //Get title
            Node titleNode = XMLUtilities.getSubNode(gamenode, "title");
            if (titleNode == null) throw new RuntimeException("Game tag didn't contain a title");
            title = XMLUtilities.getNodeText(titleNode);


            // Below tags are optional
            List<Node> tableNodeList = XMLUtilities.getSubNodes(gamenode, "table"); // Get table links from details file
            if (tableNodeList.size()>0)
            {
                for (int i=0; i<tableNodeList.size(); i++)
                {
//                    Log.i("Game", String.format("Attributes: %s", getNodeAttributes(tableNodeList.get(i))));
                    Map<String, String> tableAttr = XMLUtilities.getNodeAttributes(tableNodeList.get(i));
//                    tableFiles.put(tableAttr.get("file"), tableNodeList.get(i).getNodeName()); // Basically inserts   <table file="x">name</table>   as Map<x : name>
                    String tFilename = tableAttr.get("file");
                    String tName = XMLUtilities.getNodeText(tableNodeList.get(i));
//                    Log.i("Game.readDetailsFile", String.format("Table filename: %s", tFilename));
//                    Log.i("Game.readDetailsFile", String.format("Table name: %s", tName));
                    tableFiles.put(tName, tFilename); // Basically inserts   <table file="x">name</table>   as Map<name : x>
                }
            }

            // TODO this is where talent tree descriptors should be handled

            TalentTree tree = TalentTree.getDummy("Dummy", 3, 3);
            addTalentTree(tree);

            List<Node> talentNodeList = XMLUtilities.getSubNodes(gamenode, "tree");
            for (Node n: talentNodeList)
            {
                // Load the file that it points to
                String filename=XMLUtilities.getNodeAttributes(n).get("file");
                InputStream treeIS = loadFilesFromZip(filename).get(filename);
                String treeName = XMLUtilities.getNodeText(n);
                tree = new TalentTree(treeIS, this, treeName);
                addTalentTree(tree);
            }


        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
        }
    }

    public void addTalentTree(TalentTree t)
    {
//        if (talentTrees==null) talentTrees=new HashMap<>();
        talentTrees.put(t.name, t);
    }

    public TalentTree getTalentTreeByName(String name)
    {
//        if (talentTrees==null) talentTrees=new HashMap<>();
        if (talentTrees.containsKey(name))
        {
            return talentTrees.get(name);
        }
        throw new RuntimeException(String.format("Talent tree %s not found in game. Only have %s", name, talentTrees.keySet()));
//        return null; // TODO fix this
    }

    /**
     * Does not return set only list
     * @return
     */
    public List<String> getTalentTreeNames()
    {
        List<String> ret = new LinkedList<>();
        for (String name: talentTrees.keySet())
        {
            ret.add(name);
        }
        return ret;
    }

    public boolean hasTables()
    {
        if (tableFiles.size()>0) return true;
        return false;
    }

    public Set<String> getListOfDetailInfoTableNames()
    {
        return tableFiles.keySet();
    }

    public DetailInfoTable getDetailInfoTable(String name)
    {
        if (name.equalsIgnoreCase("test table")) // TODO this is for testing, creates dummies
        {
            DetailInfoTable dti = new DetailInfoTable(name);
            dti.addColumn("Name", TableCellType.TEXT);
            dti.addColumn("Icon", TableCellType.IMAGE);
            dti.addColumn("Value", TableCellType.NUMBER);
            return dti;
        }

        if (!getListOfDetailInfoTableNames().contains(name)) throw new IllegalArgumentException("The table by that name doesn't exist");

        if (!detailTables.containsKey(name))
        {
            // Detail table hasn't been loaded, we do that here and insert

            // First get table file name
            String filename = tableFiles.get(name);
            try {
                InputStream tableIS = loadFilesFromZip(filename).get(filename);
                Table t = new Table(tableIS, this);
                detailTables.put(name, t); // detailTables actually contains Table classes which you can use to get DITs from
            } catch (IOException e) {
                e.printStackTrace(); //TODO
            }
        }
        Table t = detailTables.get(name);
        if (t==null) throw new RuntimeException("Error while handling table " + name);
        return detailTables.get(name).getDetailInfoTable();
    }

    public int getTableCount()
    {
        return tableFiles.size();
    }

    /**
     * Called once the cover image has been found in the zip
     * @param is The input stream pointing at the cover image of the game
     */
    private void readCoverFile(InputStream is)
    {
        cover = BitmapFactory.decodeStream(is);
    }

    public Palette getPalette()
    {
        if (coverPalette==null)
            coverPalette=Palette.generate(cover);
        return coverPalette;
    }

    public String getTitle() {
        return title;
    }

    public Bitmap getCover() {
        return cover;
    }

    public ColorTheme getColorTheme()
    {
        return theme;
    }

    public int getID()
    {
        return id;
    }

    public static int getGameCount()
    {
        return idTable.size();
    }

    public static Game getGameByID(int ID)
    {
        if (idTable.containsKey(ID))
        {
            return idTable.get(ID);
        }
        throw new RuntimeException("Game requested for unknown id: " + ID);
    }
}
