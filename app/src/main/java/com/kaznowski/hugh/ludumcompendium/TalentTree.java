package com.kaznowski.hugh.ludumcompendium;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.security.acl.Group;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by hugh on 03/05/15.
 */
public class TalentTree
{
    public String name;

    HashMap<String, Talent> idTalentMap;

    private TalentTree() {}

    public TalentTree(InputStream is, Game g, String name)
    {
        idTalentMap=new HashMap<>();
        this.name = name;
        try {
            Document xml = XMLUtilities.getDocumentFromStream(is);
            Node tree = xml.getElementsByTagName("tree").item(0); // TODO Assuming only 1 tree per file
            List<Node> talentNodes = XMLUtilities.getSubNodes(tree, "talent");

            for (Node n: talentNodes) readTalent(n);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException("Something happened");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Something happened");
        } catch (SAXException e) {
            e.printStackTrace();
            throw new RuntimeException("Something happened");
        }
    }
    public void readTalent(Node n) // TODO void?
    {
        Talent t = new Talent(n);
        idTalentMap.put(t.id, t);
    }

    public List<Talent> getLayer(String group, int layer)
    {
        List<Talent> ret=new LinkedList<>();
//        Log.i("TalentTree.getLayer", String.format("values: %s", idTalentMap.values()));
        for (Talent t: idTalentMap.values())
        {
            if (t.group.equalsIgnoreCase(group)&(t.layer==layer)) ret.add(t);
        }
        return ret;
    }

    /**
     * Get the available groups of talents from the tree. A group is a collection of related talents.
     * For example a single "class" of player could have 3 talent groups: one for close combat, ranged and tactical/utility
     * The idea is to display them together and possibly and perhaps colour the background differently.
     * @return returns a list. Its easier to handle than a Set
     */
    public List<String> getGroups()
    {
        List<String> groups = new LinkedList<>();
        for (Talent t: idTalentMap.values())
        {
            if (!groups.contains(t.group)) groups.add(t.group);
        }
        Collections.sort(groups);
        return groups;
    }

    /**
     * See getGroups, because its similar
     * @return
     */
    public List<Integer> getLayers()
    {
        List<Integer> layers= new LinkedList<>();
        for (Talent t: idTalentMap.values())
        {
            if (!layers.contains(t.layer)) layers.add(t.layer);
        }
        Collections.sort(layers);
        return layers;
    }

    public static TalentTree getDummy(String name, int sizeX, int sizeY)
    {
        TalentTree tree = new TalentTree();
        tree.name=name;
        tree.idTalentMap = new HashMap<>();
        int tId=0;
        for (String gr: new String[] {"Group 1", "Group 2", "Group 3"}) {
            for (int i = 0; i < 9; i++) {
//                Log.i("TalentTree.getDummy", "Group: "+gr);
                Talent t = Talent.getDummy("Talent-" + i, "t_" + tId++, gr, i / 3);
//                Log.i("TalentTree.getDummy", String.format("Created talent with %s group and %d layer", gr, i/3));
                tree.idTalentMap.put(t.id, t);
            }
        }
        List<Talent> testList=tree.getLayer("Group 1", 0);
//        Log.i("TalentTree.getDummy", String.format("List: %s", testList));
        return tree;
    }
}
