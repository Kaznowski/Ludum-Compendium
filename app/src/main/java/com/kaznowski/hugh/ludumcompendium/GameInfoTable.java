package com.kaznowski.hugh.ludumcompendium;

import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class GameInfoTable extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_info_table);
        TableLayout tableLayout = (TableLayout) findViewById(R.id.game_info_table_layout);
        Bundle b = getIntent().getExtras();

        // TODO Get the game from bundle
        //savedInstanceState.get("Game"); //?
        int gId = (Integer) b.getInt("gameID");
        Game g=Game.getGameByID(gId);
        String tableName = b.getString("tableName");

        TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llParams.setMargins(10, 10, 10, 10); // In pixels, converting to dp is a pain
//        layoutParams.setMargins(10, 10, 10, 10);
//        ViewGroup.LayoutParams layoutParamsVG = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout rootLayout = (LinearLayout) findViewById(R.id.game_info_table_root_layout);
        rootLayout.setBackgroundColor(g.getColorTheme().bodyColour);
//        tableLayout.setBackgroundColor(g.getColorTheme().bodyColour);

        Drawable d = getResources().getDrawable(R.drawable.info_table_border); // This drawable is used for drawing the borders between cells in the table
        d.setColorFilter(g.getColorTheme().titleColour, PorterDuff.Mode.MULTIPLY); // We want the colour to be of the background of the title

        // First row is the table column description
        DetailInfoTable dit= g.getDetailInfoTable(tableName);
        TableRow hRow = new TableRow(this);
        hRow.setLayoutParams(layoutParams);
        for (String cName: dit.columnOrder)
        {
            TextView t= new TextView(this);
//            t.setText(cName+ " " + dit.columnTypes.get(cName));
            t.setText(cName);
            t.setTextColor(g.getColorTheme().titleText);
            t.setBackgroundColor(g.getColorTheme().titleColour);
  //          t.setBackground(d);
//            t.setMaxWidth(500);
//            t.setLayoutParams(llParams); // Causes table header to disappear
            t.setPadding(10, 0, 0, 0);
            hRow.addView(t);
        }
        tableLayout.addView(hRow);

        // For every row in the table, create the content
        // TODO dummy
        for (int i=0; i<dit.rows.size(); i++)
        {
            TableRow row = new TableRow(this);
            row.setLayoutParams(layoutParams);

            Object[] fields = dit.rows.get(i);

//            for (String cName: dit.columnOrder)
            for (int cIndex=0; cIndex<dit.columnOrder.size(); cIndex++)
            {
                String cName = dit.columnOrder.get(cIndex);
                TableCellType type = dit.columnTypes.get(cName);
                View v=null;
                if (type == TableCellType.TEXT)
                {
                    v = new TextView(this);
                    TextView t = (TextView) v; // Saves trouble of casting on each line
//                    t.setText("This is row " + i + " for game " + g.getTitle());
                    t.setText((String) fields[cIndex]);
                    t.setTextColor(g.getColorTheme().bodyText);
//                    t.setMaxWidth(500);
                }
                else if (type == TableCellType.IMAGE)
                {
                    v = new ImageView(this);
//                    ImageView img = new ImageView(this);
                    ImageView img = (ImageView) v;
                    if (fields[cIndex]==null) fields[cIndex] = getResources().getDrawable(R.drawable.abc_btn_radio_material);
                    img.setImageDrawable((Drawable) fields[cIndex]); // If this fails its because Bitmap isnt Drawable, but can be cast so
//                    img.setPadding(0, 0, 0, 0);
                }
                else if (type == TableCellType.NUMBER)
                {
                    v = new TextView(this);
                    TextView t2 = (TextView) v;
//                    t2.setText("" + i);
                    t2.setText(""+fields[cIndex]);
                    t2.setTextColor(g.getColorTheme().bodyText);
                }
                else
                {
                    throw new RuntimeException("Unknown TableCellType for detailInfoTable="+dit.name);
                }
//                row.setBackground(d);
//                v.setLayoutParams(llParams); // This causes height of cells to be broken
                v.setPadding(10, 10, 10, 10);
                row.addView(v);
            }

            tableLayout.addView(row, layoutParams);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game_info_table, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
