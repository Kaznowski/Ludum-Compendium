package com.kaznowski.hugh.ludumcompendium;

import org.w3c.dom.Node;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by hugh on 03/05/15.
 */
public class Talent
{
    public String name;
    public String[] description; // Talent has as many levels as there are descriptions (each description is the talent level)
    public String id; // XML-based unique identifier for talent
    public String imgFilename=null; // If no image, replace with default icon button
    public HashMap<String, Integer> requirements; // TalentID:required level
    public List<String> descriptions;
    public String group;
    public int layer;

    private Talent() {}

    public Talent(Node n)
    {
        Map<String, String> attr = XMLUtilities.getNodeAttributes(n);
        name = attr.get("name");
//        name = XMLUtilities.getNodeText(n);
        id = attr.get("id");
        imgFilename = attr.get("img");
        group = attr.get("group");
        layer = Integer.parseInt(attr.get("layer"));
        requirements=new HashMap<>();
        descriptions = new LinkedList<>();


        List<Node> descriptions = XMLUtilities.getSubNodes(n, "description");
        Collections.sort(descriptions, new Comparator<Node>() {
            @Override
            public int compare(Node lhs, Node rhs) {
                int llevel = Integer.parseInt(XMLUtilities.getNodeAttributes(lhs).get("level"));
                int rlevel = Integer.parseInt(XMLUtilities.getNodeAttributes(rhs).get("level"));
                return 0;
            }
        });

        this.descriptions=new LinkedList<>();
        for (Node d: descriptions)
        {
            this.descriptions.add(XMLUtilities.getNodeText(d));
        }

        List<Node> req = XMLUtilities.getSubNodes(n, "require");
        for (Node r: req)
        {
            Map<String, String> m = XMLUtilities.getNodeAttributes(r);
            int requiredLevel = Integer.parseInt(m.get("level"));
            String requireTalentID = m.get("talentID");
            requirements.put(requireTalentID, requiredLevel);
        }
    }

    public static Talent getDummy(String name, String id, String group, int layer)
    {
        Talent t=new Talent();
        t.name = name;
        t.id = id;
        t.group=group;
        t.layer=layer;
        t.requirements=new HashMap<>();
        t.descriptions = new LinkedList<>();

        for (int i=0; i<3; i++)
        {
            t.descriptions.add(String.format("This is description %d", i));
        }

        return t;
    }

    @Override
    public String toString() {
        return String.format("Talent<%s:%s, %s, %d>", name, id, group, layer);
    }
}
